public class ActorSivakumar implements Actor
{
public ActorSivakumar(int age,String car)
	{
	this.age=age;
	this.car=car;
	}
	int age;
	String car;
 static String address= "Coimbatore";
	public void act()
	{
	System.out.println("Tamil actor ");
	}
	public void dance()
	{
	System.out.println("Good Dancer" );
	}
	public void sing()
	{
	System.out.println("singer");
	}
	public void speaking()
	{
	System.out.println("Good speaker");
	}
	public static void main(String args[])
	{
	ActorSivakumar as = new ActorSivakumar(65, "Audi_Car");
	Actor ac = new ActorSivakumar(36,"BMW");
	as.act();
	as.dance();
	as.sing();
	as.speaking();
	System.out.println(ActorSivakumar.address);
	ac.act();
	ac.dance();
	ac.sing();
	//ac.speaking();
	}
	/* Here according to dynamic binding concept the Super class Actor can create a object in Subclass ActorSivakumar memory 
	1. it can Access the methods of its own class from subclasss 
	2. it can Access the methods that are overriden in the Subclass 
	3. it cannot access the methods that are only belongs to the Subclass by Dynamic bindibg concept
	
	*/
}
