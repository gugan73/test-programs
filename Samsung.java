//child or subclass of FactoryDemo
public class Samsung extends FactoryDemo
{
static int price=50000;
public static void main(String args[])
{
Samsung sam=new Samsung();
sam.browse();
System.out.println(sam.price);
sam.call(60);
sam.sendmessage();
sam.verifyfingerprint();
sam.providepattern();
}
//grand parent
public int call(int seconds){
return seconds;
}
public void sendmessage(){System.out.println("Send sms");}
public void recivecall(){System.out.println("recive a call");}
//parent
public void verifyfingerprint(){System.out.println("fingerprint verification");}
public void providepattern(){System.out.println("draw correct pattern");}
}